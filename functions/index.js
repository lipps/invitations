const { request } = require('https')
const parser = require('lambda-multipart-parser')
const cors = origin => ({
	'access-control-allow-origin': '*',
	'cache-control': 'no-store, private',
	'amp-access-control-allow-source-origin': origin
})
const call = (options, body = '') =>
	new Promise((resolve, reject) => {
		const req = request(options, res => {
			let body = ''
			res.on('end', () => {
				resolve(body)
			})
			res.on('data', chunk => {
				body = body + chunk
			})
		})

		req.on('error', e => {
			reject(e)
		})
		req.write(body)
		req.end()
	})

exports.handler = async event => {
	const { TEAM_ID, USER_NAME, PASSWORD, DOMAIN } = process.env
	const { queryStringParameters } = event
	const { __amp_source_origin = '' } = queryStringParameters
	const result = await parser.parse(event)
	const { account: _account } = result
	const account = _account.replace('@lipps.co.jp', '')

	const token = await call(
		{
			hostname: 'api.getshifter.io',
			path: `/v1/login`,
			method: 'POST'
		},
		JSON.stringify({ username: USER_NAME, password: PASSWORD })
	)
	const { AccessToken } = JSON.parse(token)

	const res = await call(
		{
			hostname: 'api.getshifter.io',
			path: `/v2/teams/${TEAM_ID}/members`,
			method: 'POST',
			headers: {
				Authorization: AccessToken
			}
		},
		JSON.stringify({ members: [{ email: `${account}@${DOMAIN}` }] })
	)
	if (res instanceof Error) {
		return {
			statusCode: 500,
			headers: cors(__amp_source_origin),
			body: JSON.stringify({ account, succeeded: false })
		}
	}
	return {
		statusCode: 200,
		headers: cors(__amp_source_origin),
		body: JSON.stringify({ account, succeeded: true })
	}
}
